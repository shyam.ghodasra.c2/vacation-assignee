getPeople();

var myHtml = ' <div class="yui-gd"> '
		+ '	<div class="yui-u first"><label for="startDate">Start Date</label>:</div> '
		+ ' <div class="yui-u"><input type="datetime-local" id="startDate" name="startDate"> '
		+ ' </div> '
		+ '	<div class="yui-u first"><label for="endDate">End Date</label>:</div> '
		+ ' <div class="yui-u"><input type="datetime-local" id="endDate" name="endDate"> '
		+ ' </div> '
		+ '	<div class="yui-u first"><label for="assignee">Assignee</label>:</div> '
		+ ' <div class="yui-u">'
		+ ' <select name="assignee" id="assignee"> '
		+ ' <option value="" selected="selected" disabled>Select Assignee</option> '
		
		+ ' </select> '
		+ '</div>'
		
		+ '	<div class="yui-u first"><label for="comment">Comment</label>:</div> '
		+ ' <div class="yui-u"><input type="textarea" id="comment" name="comment"> '
		+ ' </div> ' + '</div>';

function showForm() {
	var downloadDlg = new YAHOO.widget.SimpleDialog(this.id
			+ 'scheduleInterview', {
		modal : true,
		fixedcenter : true,
		draggable : true,
		constraintoviewport : true,
		width : "50em"
	});

	downloadDlg.setHeader("Vacation Assignee");

	// reqListFetch(reqposition);
	// selectinterviewer();
	//myHtml

	downloadDlg.setBody(myHtml);

	buttons = [
			{
				text : "Submit",
				handler : function() {
					console.log("After Submit");
					var startingDate = YAHOO.util.Dom.get("startDate").value;
					console.log("startingDate");
					var endingDate = YAHOO.util.Dom.get("endDate").value;
					var assignee = YAHOO.util.Dom.get("assignee").value;
					/*var workflow = YAHOO.util.Dom.get("workflow").value;*/
					var comment = YAHOO.util.Dom.get("comment").value;
					this.destroy();
					var formParams = {
						"startingDate" : startingDate,
						"endingDate" : endingDate,
						"assignee" : assignee,
						/*"workflow" : workflow,*/
						"comment" : comment
					};
					Alfresco.util.Ajax.request({
						url : Alfresco.constants.PROXY_URI
								+ "/hackathon/post-vacation-access-form-data",
						method : Alfresco.util.Ajax.POST,
						dataObj : formParams,
						requestContentType : Alfresco.util.Ajax.JSON,
						successCallback : {
							fn : function(res) {
								displayPopup("Success Message..");
								// location.reload();
							},
							scope : this

						},
						failureCallback : {
							fn : function(res) {
								displayPopup("Something went worng...");
							},
							scope : this
						}
					});

				}
			}, {
				text : "Cancel",
				handler : function() {
					this.destroy();
				}
			} ];

	downloadDlg.cfg.queueProperty("buttons", buttons);
	downloadDlg.render(document.body);
}

function getPeople() {
	Alfresco.util.Ajax.request({
		method : Alfresco.util.Ajax.GET,
		url : Alfresco.constants.PROXY_URI + "api/people",
		successCallback : {
			fn : function(response) {
				console.log("11")
				console.log(response);
				console.log(response.json.people[0].userName,
						response.json.people.length);
				for (var i = 0; i < response.json.people.length; i++) {
					console.log('i', response.json.people[i].userName);
					var split = myHtml.split('Select Assignee</option>');
					myHtml = split[0]+'Select Assignee</option> <option value="'+response.json.people[i].userName+'">'+response.json.people[i].userName+'</option>'+split[1];
					//var select = document.getElementById("assignee");
					//select.options[i] = response.json.people[i].userName;

				}

				

			},
			scope : this
		},
		failureCallback : {
			fn : function(response) {
				Alfresco.util.PopupManager.displayMessage({
					text : "Failure To get StarXpert Workflow content"
				});
			},
			scope : this
		}
	});
}

function displayPopup(message) {
	Alfresco.util.PopupManager.displayMessage({
		title : "Submitted",
		text : message
	});
}
