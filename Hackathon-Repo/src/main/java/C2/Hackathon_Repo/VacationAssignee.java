package C2.Hackathon_Repo;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.swing.text.html.parser.ContentModel;

import org.alfresco.repo.model.Repository;
import org.alfresco.repo.nodelocator.CompanyHomeNodeLocator;
import org.alfresco.repo.processor.BaseProcessorExtension;
import org.alfresco.service.ServiceRegistry;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.namespace.QName;
import org.omg.CORBA.RepositoryIdHelper;

public class VacationAssignee extends BaseProcessorExtension {

	private ServiceRegistry serviceRegistry;
	private Repository repository;

	public Repository getRepository() {
		return repository;
	}

	public void setRepository(Repository repository) {
		this.repository = repository;
	}

	public ServiceRegistry getServiceRegistry() {
		return serviceRegistry;
	}

	public void setServiceRegistry(ServiceRegistry serviceRegistry) {
		this.serviceRegistry = serviceRegistry;
	}

	public static final String VACATION_ASSIGNEE_MODEL_URI = "http://www.vacationAccess.com/model/content/1.0";
	public static final QName START_DATE = QName.createQName(VACATION_ASSIGNEE_MODEL_URI, "startDate");
	public static final QName END_DATE = QName.createQName(VACATION_ASSIGNEE_MODEL_URI, "endDate");
	public static final QName NEW_ASSIGNEE = QName.createQName(VACATION_ASSIGNEE_MODEL_URI, "assignee");

	public String getAssigneeValue(String currentUserName, Date completedDate) {
		try {

			// String luceneQuery = "PATH:\"/app:company_home//.\" AND
			// ASPECT:\"va:vacationAccess\"";
			NodeRef companyHome = repository.getCompanyHome();
			System.out.println("completedDate is:" + completedDate);
			List<String> nodes = new ArrayList<String>();
			nodes.add(currentUserName);
			NodeRef where = serviceRegistry.getFileFolderService().resolveNamePath(companyHome, nodes).getNodeRef();
			Serializable sDate = serviceRegistry.getNodeService().getProperty(where, START_DATE);
			Date startDate  = (Date) serviceRegistry.getNodeService().getProperty(where, START_DATE);
			Date endDate  = (Date) serviceRegistry.getNodeService().getProperty(where, END_DATE);
//			System.out.println("sd is:" + sd);
//			System.out.println("ed is:" + ed);
			System.out.println("sDate is:" + sDate);
			Serializable eDate = serviceRegistry.getNodeService().getProperty(where, END_DATE);
			System.out.println("eDate is:" + eDate);
//			Date startDate=getFormattedDate((String) sDate);
			System.out.println("startDate is:" + startDate);
//			Date endDate=getFormattedDate((String) eDate);
			System.out.println("endDate is:" + endDate);
			String assignee = serviceRegistry.getNodeService().getProperty(where, NEW_ASSIGNEE).toString();
			System.out.println("assignee is:" + assignee);
			if (startDate.compareTo(completedDate) * completedDate.compareTo(endDate) > 0) {
				System.out.println("In IF" + assignee);
				return assignee;
			} else {
				System.out.println("In ELSE" + currentUserName);
				return currentUserName;
			}

		} catch (Exception ex) {
			System.out.println("Exception is:" + ex);
		}
		return currentUserName;
	}

	public Date getFormattedDate(String inputDate) throws ParseException {
		Date date1 = new SimpleDateFormat("dd/MM/yyyy").parse(inputDate);
		return date1;
	}
}
